package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Main {
    /**
     * Simple server implementation taken from:
     * https://medium.com/martinomburajr/java-create-your-own-hello-world-server-2ca33b6957e (Doesn't work with Postman)
     * https://stackoverflow.com/questions/52810480/self-created-http-server-cannot-handle-postman-request (Works with postman)
     *
     * Concepts explanations: https://openclassrooms.com/fr/courses/2654601-java-et-la-programmation-reseau
     *
     * @param args
     */
    public static void main(String[] args) {
        // We will use PORT 9991
        try (ServerSocket serverSocket = new ServerSocket(9991)) {
            while (true) {
                Socket connectionSocket = serverSocket.accept();
                // Making our Server Interactive
                InputStream inputToServer = connectionSocket.getInputStream();
                OutputStream outputFromServer = connectionSocket.getOutputStream();
                BufferedReader bufferedReader = new BufferedReader(new StringReader("A Message from server."));

                try {
                    // Header should be ended with '\r\n' at each line
                    outputFromServer.write("HTTP/1.1 200 OK\r\n".getBytes());
                    //outputFromServer.write("Main: OneServer 0.1\r\n".getBytes());
                    outputFromServer.write("Content-Length: 22\r\n".getBytes()); // if text/plain the length is required
                    outputFromServer.write("Content-Type: text/plain\r\n".getBytes());
                    //outputFromServer.write("Connection: close\r\n".getBytes());

                    // An empty line is required after the header
                    outputFromServer.write("\r\n".getBytes());

                    //Print a message in the body
                    outputFromServer.write(bufferedReader.readLine().getBytes());

                    outputFromServer.flush();
                    outputFromServer.close(); // Socket will close automatically once output stream is closed.
                } catch (SocketException e) {
                    // Handle the case where client closed the connection while server was writing to it
                    connectionSocket.close();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
